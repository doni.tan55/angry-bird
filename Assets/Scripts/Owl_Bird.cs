using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Owl_Bird : Bird
{
    
    public float ImpactArea;
    public float Force;
    public LayerMask ImpactLayer;

    public bool exploded = false;

    public void explotion()
    {
        Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, ImpactArea, ImpactLayer);
        foreach(Collider2D obj in objects)
        {
            Vector2 direction = obj.transform.position - transform.position;
            obj.GetComponent<Rigidbody2D>().AddForce(direction * Force);
        }
        exploded = true;
        
    }

    private void OnCollisionEnter2D(Collision2D collider)
    {
        if(collider.gameObject.CompareTag("Enemy") || collider.gameObject.CompareTag("Obstacle"))
        {
            
            explotion();
        }
    }


   
}
